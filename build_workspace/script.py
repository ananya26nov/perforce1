"""Main script to get changes from server and build"""
import configparser
from datetime import datetime
import os
import fnmatch
import subprocess
import smtplib, ssl
from P4 import P4, P4Exception
from workspace.models import *

base_path = os.path.join(os.path.split(os.path.abspath(__file__))[0])
# Getting data from config file
config = configparser.RawConfigParser()
config.read(os.path.join(base_path, "config.config"))
smtp_dict = dict(config.items('SMTP'))
p4_dict = dict(config.items('P4'))

# Creating P4 object to communicate with P4 server
p4 = P4()
p4.port = p4_dict["p4d_ip"]+ ":" + p4_dict["p4d_port"]
p4.user = p4_dict["p4_user"]
p4.client = p4_dict["p4_client"]
p4.exception_level = 1

# Generating timestamp
time_ran = datetime.today().strftime('%Y/%m/%d:%H:%M:%S')


def update_last_run_time(time_ran):
    """
    Updates last script-run time in db
    :param time_ran: datetime
    :return: None
    """
    db_entry = Script(timestamp=time_ran)
    db_entry.save()


def get_last_run_time():
    """
    Get last script-run time from db
    :return: datetime or None
    """
    db_entry = list(Script.select().order_by(Script.id).execute())
    if len(db_entry) > 0:
        return db_entry[-1].timestamp
    else:
        return None


def send_mail(server, port, sender_email, password, receiver_email, message):
    """
    Sends email based on input
    :param server: smtp server host
    :param port: smtp server port
    :param sender_email: sender email id
    :param password: password of sender
    :param receiver_email: receiver email id
    :param message: body of email
    :return: None
    """
    email_message = """\
    Subject: Auto email : Build failed!

    %s""" % message
    context = ssl.create_default_context()
    with smtplib.SMTP(server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, email_message)

# Get last script run time
try:
    last_run = get_last_run_time()
    if not last_run:
        last_run = "1999/01/01:00:00:00"
except Exception as exp:
    raise exp

# Connect tp P4 server
try:
    p4.connect()
    change_found = p4.run("changes", "-s","submitted", "//depot/..."+ "@" + last_run + ",@" + time_ran)
    changes = p4.run_sync()
    change_desc = []
    for item in change_found:
        change_desc.append("Path : " + item["path"] + " Description : " + item["desc"])
    p4.disconnect()
except P4Exception as exp:
    raise exp
# Get build script
if os.name == 'nt':
    build_regex = 'build.bat'
else:
    build_regex = 'build.sh'
build_files_matched = fnmatch.filter(os.listdir(base_path), build_regex)
if len(build_files_matched) < 1:
    raise Exception("No build scripts found!")
elif len(build_files_matched) > 1:
    raise Exception("Multiple build files found!")

p = subprocess.Popen(os.path.join(base_path,build_files_matched[0]))
stdout, stderr = p.communicate()
# On failure
if p.returncode != 0:
    smtp_server = smtp_dict["smtp_server"]
    smtp_port = smtp_dict["smtp_port"]
    sender = smtp_dict["sender_email"]
    passwd = smtp_dict["password"]
    receiver = smtp_dict["receiver_email"]
    msg = "\n".join(change_desc)
    send_mail(smtp_server, smtp_port, sender, passwd, receiver, msg)
    raise Exception("Build failed. Reason: ", stderr)

# Submit changelist
to_change_files = os.listdir(os.path.join(base_path, "bin"))
file_paths_list = []
p4.connect()
for file in to_change_files:
    file = os.path.join(os.path.join(base_path, "bin"), file)
    file_paths_list.append(file)
    p4.run("add", "-c", "default", file)
change = p4.fetch_change()
change._description = "Build script output at" + str(datetime.today().strftime('%Y/%m/%d:%H:%M:%S'))
change._files = file_paths_list
p4.run("submit", "-d", "Build script output at" + str(datetime.today().strftime('%Y/%m/%d:%H:%M:%S')))

# Update script run time in db
try:
    update_last_run_time(time_ran)
except Exception as exp:
    raise exp
