"""Holds Schema for the database"""
from peewee import *

db = SqliteDatabase('sqlite_db.db')


class BaseModel(Model):
    """Defines the basemodel of db"""
    class Meta:
        """Defines class metadata"""
        database = db


class Script(BaseModel):
    """Defines Script table schema"""
    timestamp = CharField(null=True)
