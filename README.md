## What is it ?

This is a CI script that does the following

- Get smtp and perforce info from a config file
- Get changelist from perforce server
- Invoke a build script which produces built artifacts
- If build is successful it will update  the artifacts back in the perforce
- In case of execution error it will send an email with the changelog
    
The main build script is ```script.py``` It gets p4 and smtp details from config file and fetches changess from p4d server.
The change must contain a build file (```build.bat``` for windows and ```build.sh``` for unix). The build.bat provided here is an example to create dummy files under ```bin``` directory.
The script then invokes this build file and checks for return code. On success it adds  the files produced under bin to default changelist with a message and submits it.
On failure it sends a mail with details of the changelist.

The file ```initialize.py``` is to initialize an sqlite db for the first time to capture the script run time.
The db generated is called ```sqlite_db.db``` The schema iis taken from ```models.py```.
The directory structure is:

```
|Project_root
|__p4_workspace
   |__script.py
   |__build.bat
   |__models.py
   |__initialize_db.py
   |__config.config
   |__requirements.txt
   |__README.md
|sqlite_db.db

```
## Assumption

- Build file is called ```build.bat``` for windows and ```build.sh``` for unix
- Config file is called config.config

## Limitations

- Password is expected to be in plaintext in configfile
- If using a gmail account to send email "Less secure app access" option has to be enabled in the account
- No unit tests
- SQLite db is used
- The db is created at the root level of the project and should remain outside the P4 workspace. Otherwise the error "Unable to write to read only database" may be encountered.

## Requirements
- python3
- python3-pip
- ```pip install -r requirements.txt```

## To run
- Run initialize_db.py once to initiate the database.
- Update the config file to point to correct smtp and p4 values
- Run script.py